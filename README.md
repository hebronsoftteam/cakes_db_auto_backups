# Cakes Mongodb autobackups

Automatic creation of backups by node-cron and store to aws s3 bucket

## Description

Backup creating process started by node-cron according to CRON_PATTERN specified in envs. Each collection pass validation on existing and non-null amount of documents. BackUp log saving to backuping db and contain url to aws s3 with backup file, date of creation, validation pass information.