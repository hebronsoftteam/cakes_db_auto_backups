const AWS = require('aws-sdk');
const config = require('./config');
const path = require('path');
const fs = require('fs');

const bucketName = config['AWS_S3_BUCKET_NAME'];

function _getAwsS3Instance(
	params = {
		apiVersion: '2006-03-01',
	}
) {
	AWS.config.credentials = {
		accessKeyId: config['AWS_ACCESS_KEY_ID'],
		secretAccessKey: config['AWS_SECRET_ACCESS_KEY'],
	};
	return new AWS.S3(params);
}

function _getAWSRequestPromise(request) {
	return request.promise();
}

function _putObjectAWSBucket(params) {
	return _getAWSRequestPromise(_getAwsS3Instance().putObject(params));
}

function _deleteObjectAWSBucket(params) {
	return _getAWSRequestPromise(_getAwsS3Instance().deleteObject(params));
}

function _getFile(filePath) {
	return new Promise((resolve, reject) => {
		fs.readFile(filePath, (err, data) => {
			if (err) {
				reject(err);
			}
			resolve(data);
		});
	});
}

module.exports = async (filePath) => {
	try {
		const fileBuffer = await _getFile(filePath);
		await _putObjectAWSBucket({
			Bucket: bucketName,
			Key: path.basename(filePath),
			Body: fileBuffer,
			ACL: config['AWS_S3_OBJECT_ACL'],
		});
		return `https://${bucketName}.s3.amazonaws.com/${path.basename(filePath)}`;
	} catch(err) {
		throw new Error(err);
	}
};
