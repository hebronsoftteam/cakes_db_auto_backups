const dotenv = require('dotenv');
const fs = require('fs');
let config;

if (process.env.NODE_ENV) {
    config = dotenv.parse(fs.readFileSync(`configs/${process.env.NODE_ENV}.env`));
} else {
    throw new Error('NODE_ENV needs to be provided');
}

module.exports=config;