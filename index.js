const node_cron = require('node-cron');
const mongodb_backup = require('mongodb-backup-4x');
const { MongoClient } = require('mongodb');
const { execSync } = require('child_process');
const mkdirp = require('mkdirp');
const rimraf = require('rimraf');
const config = require('./config');
const aws_file_upload = require('./aws-file-upload');

const uri = config['mongodb_uri'];
const dbName = config['mongodb_name'];
const fullUri = `${uri}/${dbName}`;
const client = new MongoClient(fullUri, { useUnifiedTopology: true });
mkdirp.sync('./backups');
setupLogCollection();

node_cron.schedule(config['CRON_PATTERN'], async () => {
	try {
		const excludeCollections = ['changelog', 'backupLogs', 'deletedDocuments'];
		const collections = await client.db(dbName).listCollections({}, { nameOnly: true }).toArray();
		const validation = {
			pass: true,
			validationError: null,
		};
		for (const collection of config['collections_to_dump'].split(',')) {
			if (!collections.find((value) => value.name === collection) && !excludeCollections.includes(collection)) {
				(validation.pass = false), (validation.validationError = `${collection} does not exist`);
			}
			if (!excludeCollections.includes(collection)) {
				const amountOfDocument = await client.db(dbName).collection(collection).countDocuments();
				if (!amountOfDocument) {
					(validation.pass = false), (validation.validationError = `${collection} is empty`);
				}
			}
		}
		const currentDate = new Date();
		if (currentDate.getUTCHours() >= 12 && currentDate.getUTCHours() < 1) {
			rimraf.sync('./backups/*');
		}
		const backUpFileName = new Date().getTime();
		execSync(`mongodump --uri=${fullUri} --archive=${__dirname + '/backups/' + backUpFileName}`);
		const url = await aws_file_upload('./backups/' + backUpFileName);
		await runQuery({ backupFileUrl: url, created: new Date().toUTCString(), validation }, null);
	} catch (err) {
		throw new Error(err);
	}
});

async function runQuery(insertData, findCond) {
	try {
		const database = client.db(dbName);
		if (insertData) {
			await database.collection('backupLogs').insertOne(insertData);
		} else if (findCond) {
			return database.collection('backupLogs').find(findCond);
		}
	} catch (err) {
		throw new Error(err);
	}
}

async function setupLogCollection() {
	try {
		await client.connect();
		const database = client.db(dbName);
		const collections = await database.listCollections({ name: 'backupLogs' }, { nameOnly: true }).toArray();
		if (!collections.length) {
			await database.createCollection('backupLogs');
		}
	} catch (err) {
		throw new Error(err);
	}
}
